import express from "express";
import { v4 as generatedId } from "uuid";
import dotenv from 'dotenv';
dotenv.config();

const pets = [
  {
    id: "addak2342",
    name: "Molly",
    type: "dog",
    age: 3,
  },
  {
    id: "198ddda34",
    name: "Qui",
    type: "parrot",
    age: 1,
  },
  {
    id: "3alj29384",
    name: "Mia",
    type: "cat",
    age: 2,
  },
];

const app = express();
app.use(express.json());

app.get("/pets", (req, res) => {
  return res.json(pets);
});

app.get("/pets/:id", (req, res) => {
  return res.json(pets.find(({ id }) => id === req.params.id));
});

app.post("/pets", (req, res) => {
  const id = generatedId();
  pets.push({ ...req.body, id });
  return res.json("Successfully added!");
});

app.listen(process.env.PORT, () => {
  console.log(`${process.env.PORT} port is running...`);
});
